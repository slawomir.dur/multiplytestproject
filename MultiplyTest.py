import unittest
import unittest.mock

from Multiply import multiply

"""
You have 3 test cases below that basically do the same thing but with three different approaches.
Have fun reading.

By the way!
Your task is to test the following functions:
 - divide
 - fun_divide

You can find them in Multiply.py
"""


class MultiplyNonMockFunctionTestCase(unittest.TestCase):
    def test_multiplication(self):
        def return_two():
            return 2

        self.assertEqual(multiply(return_two, 2), 4)

        def return_four():
            return 4

        self.assertEqual(multiply(return_four, 2), 8)

        def return_six():
            return 6

        self.assertEqual(multiply(return_six, 2), 12)

        def raise_error():
            raise RuntimeError

        self.assertRaises(RuntimeError, multiply, raise_error, 2)


class MultiplyNonMockLambdaTestCase(unittest.TestCase):
    def test_multiplication(self):
        self.assertEqual(multiply(lambda: 2, 2), 4)
        self.assertEqual(multiply(lambda: 4, 2), 8)
        self.assertEqual(multiply(lambda: 6, 2), 12)
        # no raise example because you cannot raise in lambda :(


class MultiplyMockTestCase(unittest.TestCase):
    def test_multiplication(self):
        # object pretending to be a function which returns return_value
        function = unittest.mock.Mock()
        function.return_value = 2

        self.assertEqual(multiply(function, 2), 4)

        # setting return_value of mock function to 4
        function.return_value = 4
        self.assertEqual(multiply(function, 2), 8)

        function.return_value = 6
        self.assertEqual(multiply(function, 2), 12)

        # now saying that a call to mock function should raise RuntimeError
        function.side_effect = RuntimeError
        self.assertRaises(RuntimeError, multiply, function, 2)

        # checking if mock function was called 4 times during the test
        self.assertTrue(function.call_count == 4)


if __name__ == '__main__':
    unittest.main()
