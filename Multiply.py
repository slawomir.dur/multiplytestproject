
def multiply(function, value):
    """
    Multiplicator of a return value of a function by passed value
    :param function: the function that returns something that can be multiplied
    :param value: value that is used to multiply the return value of the function
    :return: The multiplied value of expression - function()*value
    """
    return function() * value


def divide(function, value):
    return function() / value


def fun_divide(function1, function2, value):
    return multiply(function1, value) / (function2() / value)
